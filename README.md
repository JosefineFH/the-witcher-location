Prosjekt for og sette meg mer inn i javascript og css. 

Dette er en onepager der det er en toTop knap som kommer opp jo lengere ned på siden. Det er også en responsive meny som fungerer fint på mobil og deskop. 
Siden er satt opp med tre section som inneholder en h1, p tag og en a tag for vidre link til egen wiki side om the witcher. 

Live demo her https://fervent-mirzakhani-2990aa.netlify.app/